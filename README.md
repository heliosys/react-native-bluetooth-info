# react-native-bluetooth-info

## Getting started

`$ npm install react-native-bluetooth-info --save`

### Mostly automatic installation

`$ react-native link react-native-bluetooth-info`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-bluetooth-info` and add `BluetoothInfo.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libBluetoothInfo.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainApplication.java`
  - Add `import com.reactlibrary.BluetoothInfoPackage;` to the imports at the top of the file
  - Add `new BluetoothInfoPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-bluetooth-info'
  	project(':react-native-bluetooth-info').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-bluetooth-info/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-bluetooth-info')
  	```


## Usage
```javascript
import BluetoothInfo from 'react-native-bluetooth-info';

// TODO: What to do with the module?
BluetoothInfo;
```
