#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface BluetoothInfo : RCTEventEmitter <RCTBridgeModule, CBCentralManagerDelegate>


@end


  